package cl.app.fbcallmonitor.backgroundservice;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.*;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cl.app.fbcallmonitor.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * Created by focmb on 25.10.13.
 */
public class Backgroundservice extends Service
{
    Handler h;
    Socket socket;
    String host;
    int port;

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {

        Bundle extras = intent.getExtras();
        String[] TCPParameter = extras.getStringArray("TCPParameter");
        host = TCPParameter[0];
        port = Integer.parseInt(TCPParameter[1]);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate()
    {
        h = new Handler();
        Toast.makeText(this, "My Service Created", Toast.LENGTH_LONG).show();
        CallMon co = new CallMon();
        co.execute();
    }

    @Override
    public void onDestroy()
    {
        if(socket.isConnected())
        {
            try
            {
                socket.close();
            }
            catch (IOException e)
            {
                Log.e(getString(R.string.app_name), Log.getStackTraceString(e));
            }
        }
        Toast.makeText(this, "My Service Stopped", Toast.LENGTH_LONG).show();
    }

    class CallMon extends AsyncTask<Void, String, String>
    {
        @Override
        protected String doInBackground(Void... params)
        {
            try
            {
                socket = new Socket();
                SocketAddress sockaddr = new InetSocketAddress(host, port);
                socket.connect(sockaddr);
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String line;
                while ((line = in.readLine()) != null)
                {
                    Log.d("CL", line);
                    if(line.contains("RING"))
                    {
                        String[] tmp = line.split(";");
                        publishProgress("Anruf von: " + tmp[3]);
                    }
                }
            }
            catch (IOException e)
            {
                Log.e(getString(R.string.app_name), Log.getStackTraceString(e));
                return null;
            }
            return null;
        }

        protected void onProgressUpdate(String... progress)
        {
            String msg = "";
            for(String s: progress)
            {
                msg += s;
            }
            final String finalMsg = msg;
            h.post(new Runnable()
            {
                @Override
                public void run()
                {
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View toastView = inflater.inflate(R.layout.toast, null);
                    ImageView iv = (ImageView) toastView.findViewById(R.id.image);
                    iv.setImageResource(R.drawable.ic_launcher);
                    TextView tv = (TextView)toastView.findViewById(R.id.text);
                    tv.setText(finalMsg);
                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastView);
                    toast.show();
                }
            });
        }

        protected void onPostExecute(String result)
        {

        }
    }
}

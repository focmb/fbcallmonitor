package cl.app.fbcallmonitor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import cl.app.fbcallmonitor.additional.additionalChecks;
import cl.app.fbcallmonitor.additional.checkFBConnectivity;
import cl.app.fbcallmonitor.backgroundservice.Backgroundservice;

/**
 * Erstellt am 25.10.13
 */

public class MainActivity extends ActionBarActivity implements View.OnClickListener
{
    boolean serviceIsRunning = false;
    Button button, button2;
    additionalChecks ac;
    String host = "fritz.box";
    int port = 1012;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeVariables();
    }

    /**
     * Initialisieren von Variablen
     */
    private void initializeVariables()
    {
        ac = new additionalChecks(this);
        button = (Button)findViewById(R.id.button);
        button.setOnClickListener(this);
        button2 = (Button)findViewById(R.id.button2);
        button2.setOnClickListener(this);
    }



    boolean hasChecked = false;
    public boolean isHasChecked()
    {
        return hasChecked;
    }

    public void setHasChecked(boolean hasChecked)
    {
        this.hasChecked = hasChecked;
    }

    boolean fbReachable = false;
    public boolean isFbReachable()
    {
        return fbReachable;
    }

    public void setFbReachable(boolean fbReachable)
    {
        this.fbReachable = fbReachable;
    }

    /**
     * Prüfen von Systemfunktionen, die für die App wichtig sind, z. B. WLAN-Verbindung
     * Prüfen, ob mit der Fritzbox verbunden
     */
    private boolean checkWifiState()
    {
        if(!ac.checkWifi())
        {
            Log.d("CL", "Keine WLAN-Verbindung");
            return false;
        }
        else if(!isHasChecked())
        {
            new checkFBConnectivity(host, port, 2000, this).execute();
            while(!isHasChecked()){}
            return isFbReachable();
        }
        else if(!isFbReachable())
            return false;
        else
            return true;
    }

    /**
     * Testbutton, um den Service zu starten und anzuhalten
     * @param v
     */
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.button:
                if(!serviceIsRunning && checkWifiState())
                {
                    Log.d("CL", "Starte Service");
                    String tcpParameter[]  = {host, String.valueOf(port)};
                    Intent serviceIntent = new Intent(MainActivity.this,Backgroundservice.class);
                    serviceIntent.putExtra("TCPParameter", tcpParameter);
                    startService(serviceIntent);
                    serviceIsRunning = true;
                }
                else
                    Log.d("CL","Service läuft bereits oder Fritzbox ist nicht auf Port 1012 erreichbar");
                break;
            case R.id.button2:
                if(serviceIsRunning)
                {
                    Log.d("CL", "Stoppe Service");
                    stopService(new Intent(this, Backgroundservice.class));
                    serviceIsRunning = false;
                }
                else
                    Log.d("CL", "Service läuft nicht");
                break;
        }
    }

    /**
     * Optionsmenüeintrage hinzufügen
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        menu.add(1, 100, 0, "Optionen");
        menu.add(1, 200, 0, "Über");
        menu.add(1, 201, 0, "Beenden");
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Optionsmenüeinträge mit Funktion versehen
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case 100:
                return true;
            case 200:
                AboutBox.Show(MainActivity.this);
                return true;
            case 201:
                exitApp();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Beenden der App mit Nachfragedialog
     */
    private void exitApp()
    {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        alertbox.setMessage( this.getString(R.string.app_name) + " beenden?");
        alertbox.setPositiveButton("Ja", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)
            {
                try
                {
                    if(serviceIsRunning)
                        stopService(new Intent(MainActivity.this, Backgroundservice.class));
                    System.exit(0);
                }
                catch(Exception ex)
                {
                    Log.e(getString(R.string.app_name), Log.getStackTraceString(ex));
                }
            }
        });
        alertbox.setNegativeButton("Nein", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)
            {
                Toast.makeText(MainActivity.this, "Aktion abgebrochen!", Toast.LENGTH_SHORT).show();
            }
        });
        alertbox.show();
    }
}

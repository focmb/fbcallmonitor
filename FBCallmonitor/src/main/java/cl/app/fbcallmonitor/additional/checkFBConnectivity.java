package cl.app.fbcallmonitor.additional;

import android.os.AsyncTask;
import android.util.Log;
import cl.app.fbcallmonitor.MainActivity;
import cl.app.fbcallmonitor.R;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * Created by focmb on 29.10.13.
 */
public class checkFBConnectivity extends AsyncTask<Void, Void, Boolean>
{
    String host;
    int port;
    int timeout;
    MainActivity ma;
    Socket socket;

    public checkFBConnectivity(String host, int port, int timeout, MainActivity ma)
    {
        this.host = host;
        this.port = port;
        this.timeout = timeout;
        this.ma = ma;
    }

    @Override
    protected Boolean doInBackground(Void... params)
    {
        try
        {
            socket = new Socket();
            SocketAddress sockaddr = new InetSocketAddress(host, port);
            socket.connect(sockaddr, timeout);
            socket.close();
            ma.setHasChecked(true);
            ma.setFbReachable(true);
            return true;
        }
        catch (IOException e)
        {
            Log.e(ma.getString(R.string.app_name), Log.getStackTraceString(e));
            ma.setHasChecked(true);
            ma.setFbReachable(false);
            return false;
        }
    }

    /**
    @Override
    protected void onPostExecute(Boolean result)
    {
        super.onPostExecute(result);
        if(socket != null && socket.isConnected())
        {
            try
            {
                socket.close();
            }
            catch (IOException e)
            {
                Log.e(ma.getString(R.string.app_name), Log.getStackTraceString(e));
            }
        }
        ma.setFbReachable(result);
        ma.setHasChecked(true);
    }
    */
}

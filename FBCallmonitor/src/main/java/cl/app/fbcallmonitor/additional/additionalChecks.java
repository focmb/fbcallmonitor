package cl.app.fbcallmonitor.additional;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;
import cl.app.fbcallmonitor.MainActivity;
import cl.app.fbcallmonitor.R;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * Created by focmb on 27.10.13.
 */
public class additionalChecks
{
    WifiManager wifiManager;
    Context c;

    public additionalChecks(Context c)
    {
        this.c = c;
        wifiManager = (WifiManager)c.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    /**
     * Prüfen, ob WiFi aktiv und verbunden
     * @return
     */
    public boolean checkWifi()
    {
        /*
        wifiManager.setWifiEnabled(true);
        wifiManager.setWifiEnabled(false)
        */
        return wifiManager.isWifiEnabled();
    }
}



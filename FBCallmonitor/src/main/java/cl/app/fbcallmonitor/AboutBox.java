package cl.app.fbcallmonitor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.SpannableString;
import android.text.util.Linkify;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by focmb on 24.10.13.
 */
public class AboutBox
{
    /**
     * Auslesen der App-Version
     * @param context
     * @return String mit der App-Version
     */
    static String VersionName(Context context)
    {
        try
        {
            return context.getPackageManager().getPackageInfo(context.getPackageName(),0).versionName;
        }
        catch (NameNotFoundException e)
        {
            return "Unbekannt";
        }
    }

    /**
     * Erstellen und Anzeigen der AboutBox
     * @param callingActivity Activty, aus der die AboutBox aufgerufen wird
     */
    public static void Show(Activity callingActivity)
    {
        SpannableString aboutText = new SpannableString("Version:\n" +
                VersionName(callingActivity) +
                "\n\n" +
                callingActivity.getString(R.string.copyright) +
                "\n\n" +
                callingActivity.getString(R.string.author) +
                "\n\n" +
                callingActivity.getString(R.string.email));
        View about;
        TextView tvAbout;
        try
        {
            LayoutInflater inflater = callingActivity.getLayoutInflater();
            about = inflater.inflate(R.layout.about, (ViewGroup) callingActivity.findViewById(R.id.aboutView));
            tvAbout = (TextView) about.findViewById(R.id.aboutText);
        }
        catch(InflateException e)
        {
            about = tvAbout = new TextView(callingActivity);
        }
        tvAbout.setText(aboutText);
        Linkify.addLinks(tvAbout, Linkify.ALL);
        new AlertDialog.Builder(callingActivity)
                .setTitle("Über " + callingActivity.getString(R.string.app_name))
                .setCancelable(true)
                .setIcon(R.drawable.ic_launcher)
                .setPositiveButton("OK", null)
                .setView(about)
                .show();
    }
}


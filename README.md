# Fritzbox-Callmonitor für Android


Diese Android-App zeigt auf einer Fritzbox ankommende Anrufe als kleines Popup auf dem Smartphone an. Dazu muss auf der FritzBox der Callmonitor aktiviert sein. Um den Callmonitor auf der Fritzbox zu aktivieren, muss folgendes über das Telefon eingegeben werden:

Zum Aktivieren:

```sh
#96*5*
```

Zum Deaktivieren:

```sh
#96*4*
```

Die Fritzbox liefert dann über den Port 1012 und ihren Hostnamen bzw. ihre IP-Adresse die Telefonnummer des Anrufers aus.

Nach dem Starten der App läuft diese als Service im Hintergrund. Falls ein Anruf registriert wird, erschein ein kleines Popup (Toast), das die Telefonnummer des Anrufers anzeigt.

Die App wurde mit Android 4.2.1 getestet, sollte aber auch 2.3 und höher laufen.

Wie unter *TODO* zu sehen ist, ist die App noch lange nicht fertig, d. h. es sind noch einige Features einzubauen. Bisher hat die App nur einen Alpha-Status, gilt also als nicht fehlerfrei.

***Diese App funktioniert nur im lokalen Netzwerk.***

---

### TODO

* Dialog zum Aktivieren des WLANs anzeigen
* WiFi-Lock einbauen, damit die WLAN-Verbindung nicht unterbrochen wird, wenn das Display ausgeht und das Telefon länger nicht benutzt wird (WifiManager.WifiLock)
* Optionsmenü erstellen, damit bspw. die Adresse der Fritzbox manuell eingetragen werden kann
* Auslesen des Namens der zur anrufenden Nummer gehört aus dem Telefonbuch der Fritzbox
* Anzeigen der Information auf dem Lockscreen
* Handling für unterbrochene Netzwerkverbindung einbauen

---

### Änderungen

***Version 1.0.2***

`ADDED`
  >-neues Icon 
  
`FIXED`    
  >-Prüfung, ob Verbindung zur Fritzbox besteht

***Version 1.0.1***

`ADDED`
  >-Prüfung, ob WLAN aktiv und Fritzbox auf Port 1012 erreichbar ist  
  >-Icon im Popup  
  >-Optionsmenü mit "Über" und "Beenden"

`FIXED`
  >-Schriftgröße im Popup

***Version 1.0.0***

`NEW`
  >-erste Version mit rudimentären Funktionen